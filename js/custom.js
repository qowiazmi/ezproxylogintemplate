/* Bootstrap Navbar dropdown on Hover*/

$('.dropdown-menu a.dropdown-toggle .dropdown-submenu').on('mouseenter mouseleave', function(e) {
    if (!$(this).next().hasClass('show')) {
    $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
    }
    var $subMenu = $(this).next('.dropdown-menu');
    $subMenu.toggleClass('show');


    $(this).parents('.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
    $('.dropdown-submenu .show').removeClass('show');
    });


    return false;
});  

$(".dropdown, .dropdown-submenu").hover(
    function () {
        $('>.dropdown-menu', this).stop(true, true).fadeIn("fast");
        $(this).addClass('open');
    },
    function () {
        $('>.dropdown-menu', this).stop(true, true).fadeOut("fast");
        $(this).removeClass('open');
    }); 

/* End Bootstrap Navbar dropdown on Hover*/